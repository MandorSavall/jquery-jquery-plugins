    /*
    // 
    const buttons = document.getElementsByClassName("btn");
    for(let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function(){
            this.style.color = "white";
            this.style.backgroundColor = "red";
        });
    }
    */ 

const buttons = document.getElementsByClassName("btn"); // найти все кнопки с помощью Vanilla JS
 console.log(buttons); // выведет HTML-коллекцию
 console.log($(".btn")); // выведет совсем не HTML-коллекцию

// более простой способ повесить событие на группу селекторов
$(".reverse-btn").click(function(){
    $(this).css({"color": "white", "background-color": "red"}); // если нужно установить несколько inline-свойств - передавайте объект с их описанием
    $(this).css("font-size", "18px"); // более понятный и близкий к HTML+CSS синтаксис
});


/*
// Другой способ повесить обработчик. В чем между ними разница?
    $(".reverse-btn").on("click", function(){
    $(this).css("color", "white");
    $(this).css("background-color", "red");
});
*/