/*
Задание: пользователь вводит в поле ввода год.
 Вывести внутри тега p с id leap-year-result ответ
 - высокосный год или нет.
 Используйте Jquery.
*/

 $("#is-leap-year").on("click", function(){
     const year = $("#year").val();
     const prefix = (isLeapYear(year)) ? "" : "не";
     $("#leap-year-result").text(`${year} - ${prefix}високосный`);
 });

 function isLeapYear (year){
     const date = new Date(year, 2, 0);
     const februaryDays = date.getDate();
     return (februaryDays === 29);
 }