/*
Пользователь вводит дело в поле ввода,
 нажимает на кнопку Add case, и оно появляется в списке.
 При наведении на дело в правом верхнем углу появвляется крестик,
 при нажатии на который дело удаляется из списка.
*/
$("#add-case").on("submit", function (e) {
    e.preventDefault();
    const caseVal = $(this).find("[name='case']").val();
    $("#case-list").append(`<li class="case-item">${caseVal}<span class="close">&times;</span></li>`);
});

$("#case-list").on("click", ".close", {selector:"li"}, deleteClosestElem);

function deleteClosestElem(e) {
    $(this).closest(e.data.selector).remove();
}