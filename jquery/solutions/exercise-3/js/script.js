/*
Напишите скрипт, который при наведении на div
с классом tooltip создает внутр него
всплывающую подсказку - span с классом tooltiptext.
Пример его работы можно посмотреть
здесь https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_tooltip
*/

$(".tooltip").hover(function() {
    // const tooltipText = $(this).attr("data-tooltip");
    const tooltipText = $(this).data("tooltip");
    $(this).append(`<span class="tooltiptext">${tooltipText}</span>`);
}, function() {
    $(this).find(".tooltiptext").remove();
});
/*
$('.tooltip').on('mouseover', function() {
    // const tooltipText = $(this).attr("data-tooltip");
    const tooltipText = $(this).data("tooltip");
    $(this).append(`<span class="tooltiptext">${tooltipText}</span>`);
});
$('.tooltip').on('mouseout', function() {
    $(this).find(".tooltiptext").remove();
});
*/
