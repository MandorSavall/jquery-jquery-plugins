/*
Напишите код, который работает так: при клике на пункте меню к нему добавляется класс active, а у всех других пунктов он удаляется. 
*/

$('#top-menu').on('click', function(e) {
    e.preventDefault();
    $(this).find('a.active').removeClass('active');
    $(e.target).addClass('active');
});
