/*
При клике на элемент с классом btn-collapse после него
ПЛАВНО должен появлятся p с текстом, храняющимся в атрибуте data-info
*/

    $('.btn-collapse').on('click', function(e) {
        e.preventDefault();
        const next = $(this).next('.collapse-text');
        if (next.length) {
            $(next).remove();
        } else {
            const dataValue = $(this).data('info');
            $(this).after(`<p class="collapse-text">${dataValue}</p>`);
            $(this).next('.collapse-text').show("slow");
        }
    });