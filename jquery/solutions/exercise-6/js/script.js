/*
При клике на кнопку с data-target = "modal" должно ПЛАВНО (со скоростью 500) появлятся всплывающее окно, селектор которого записан в атрибуте data-target.
А при клике на крестик внутри окна - оно должно ПЛАВНО (со скоростью 700) пропадать.
Используйте для считывания клика на крестик всплытие и
перехват с помощью JQuery.
*/

$("[data-type=modal]").on("click", function(){
    const selector = $(this).data("target");
    $(selector).show(500);
});

$(".modal").on("click", ".close", function(){
    const modal = this.closest(".modal");
    $(modal).hide(700);
})